const problem2 = function(inventory){


    if (inventory !== undefined && inventory.length > 0 && inventory.constructor.name === "Array") {
        inventory.sort(function(a,b){
            return a.id - b.id
        })

        return inventory[inventory.length-1]
    }
    return []
}


module.exports = problem2;