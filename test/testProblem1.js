let inventory = require("../cars")

const problem1 = require("../problem1")

let id = 33;

const result = problem1(inventory,id);

if (result.constructor.name === "Object"){
    console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`)
}else{
    console.log(`Please pass valid data to the function "problem1" to get valid output`)
}


