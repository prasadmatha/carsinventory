const problem6 = function(inventory){

    let BMWAndAudi = []

    if (Array.isArray(inventory) && inventory.length > 0){

        for (let index=0 ; index < inventory.length ; index++){

            if (inventory[index].car_make === "BMW" || inventory[index].car_make === "Audi"){
                BMWAndAudi.push(inventory[index])
            }
        }

        if (BMWAndAudi === undefined) return []

        return BMWAndAudi
    }
    
    return []
    
}
    



module.exports = problem6;