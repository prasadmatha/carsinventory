const problem3 = function(inventory){
    let carNamesList = []
    if (inventory !== undefined && inventory.length > 0 && inventory.constructor.name === "Array") {
        
        for (let index = 0 ; index < inventory.length ; index++){
            if (inventory[index].car_model !== undefined) carNamesList.push(inventory[index].car_model)
            

        }
        
        carNamesList.sort(function(a,b){
            if (a.toLowerCase() < b.toLowerCase()) return -1;
            if (a.toLowerCase() > b.toLowerCase()) return 1;
            return 0
        })
        if (carNamesList !== undefined) return carNamesList;
        return []
        

        
    }
    
    return []
}


module.exports = problem3;