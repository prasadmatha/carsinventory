const problem5 = function(inventory){

    let olderCarsYearsList = []

    if (Array.isArray(inventory) && inventory.length > 0){

        const problem4 = require("./problem4")
    
        let carsYearsList = problem4(inventory)

        for (let index = 0 ; index < carsYearsList.length ; index++){

            if (carsYearsList[index] < 2000) olderCarsYearsList.push(carsYearsList[index])

        }
        
        if (olderCarsYearsList !== undefined) return olderCarsYearsList
        return []
    }

    return []

}


module.exports = problem5;