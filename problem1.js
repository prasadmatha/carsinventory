const problem1 = function(inventory,id){
    let result;
    
    if (inventory === undefined || inventory.constructor.name !== "Array" || inventory.length === 0 || id === undefined || typeof(id) !== "number"){
        return []
    }
    else{
        for (let index = 0 ; index < inventory.length ; index++){
            if (id === inventory[index].id){
                result =  inventory[index]
            }
        }
        if (result !== undefined){
            return result 
        }
            return []
    }
        
}


module.exports = problem1;